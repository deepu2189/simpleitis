<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a class="ctr blck" href="https://blog.bulletproof.com/"><img src="img/logo-footer.svg" alt="Bulletproof Logo" width="50" height="59"></a>
                <div class="sm ctr">
                    <a class="fb ico" href="https://www.facebook.com/Bulletproof" target="_blank"></a>
                    <a class="tw ico" href="https://twitter.com/bpnutrition" target="_blank"></a>
                    <a class="yt ico" href="https://www.youtube.com/user/bulletproofexecutive/" target="_blank"></a>
                    <a class="ig ico" href="https://www.instagram.com/bulletproof/" target="_blank"></a>
                    <a class="pn ico" href="https://www.pinterest.com/bulletproofpins/" target="_blank"></a>
                    <div class="clear"></div>
                </div>
                <!-- .sm -->
                <div class="su ctrm">
                    <h2>Sign-up for the Bulletproof mailing list for the latest news and updates</h2>
                    <form action="https://cloud.mail.bulletproof.com/signup-handler" method="post">
                        <input type="hidden" name="SuccessURL" value="https://blog.bulletproof.com/thank-you-for-subscribing/">
                        <input type="hidden" name="ErrorURL" value="https://blog.bulletproof.com/">
                        <input type="hidden" id="Source" name="Source" value="Blog_Footer">
                        <input type="hidden" name="SourceURL" value="https://blog.bulletproof.com/">
                        <input type="hidden" name="RefURL" value="">
                        <input type="hidden" name="Section" value="">
                        <input type="hidden" name="Tags" value="">
                        <input type="text" id="email_field" name="Email Address" placeholder="Email" value="">
                        <input value="Submit" type="submit">
                        <div> </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row links">
            <div class="col-sm-3 menu-footer-1">
                <h2 class="goth">Get Involved</h2>
                <div class="menu-get-involved-container">
                    <ul id="menu-get-involved" class="footer-1-menu">
                        <li id="menu-item-11046" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11046"><a href="https://www.bulletproof.com/">Shop Now</a></li>
                        <li id="menu-item-8907" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8907"><a target="_blank" href="https://www.bulletproof.com/about-us">About Us</a></li>
                        <li id="menu-item-8802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8802"><a target="_blank" href="https://www.bulletproof.com/careers/">Careers</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3 menu-footer-2">
                <h2 class="goth">Help Center</h2>
                <div class="menu-help-center-container">
                    <ul id="menu-help-center" class="footer-2-menu">
                        <li id="menu-item-8807" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8807"><a target="_blank" href="https://support.bulletproof.com/">Contact Us</a></li>
                        <li id="menu-item-9272" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9272"><a href="https://blog.bulletproof.com/the-bulletproof-executive-press-room/">Press</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3 menu-footer-3">
                <h2 class="goth">Browse</h2>
                <div class="menu-browse-container">
                    <ul id="menu-browse" class="footer-3-menu">
                        <li id="menu-item-10992" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-10992"><a href="https://blog.bulletproof.com/category/article/">Articles</a></li>
                        <li id="menu-item-10993" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-10993"><a href="https://blog.bulletproof.com/category/podcasts/">Podcast</a></li>
                        <li id="menu-item-10994" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-10994"><a href="https://blog.bulletproof.com/category/recipes/">Recipes</a></li>
                        <li id="menu-item-10995" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-10995"><a href="https://blog.bulletproof.com/category/video/">Videos</a></li>
                        <li id="menu-item-25142" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25142"><a href="https://blog.bulletproof.com/events/">Events</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3 menu-footer-4">
                <h2 class="goth">Resources</h2>
                <div class="menu-resources-container">
                    <ul id="menu-resources" class="footer-4-menu">
                        <li id="menu-item-10999" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10999"><a href="https://blog.bulletproof.com/bulletproof-for-beginners/">Start Here</a></li>
                        <li id="menu-item-10998" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10998"><a href="https://blog.bulletproof.com/the-complete-illustrated-one-page-bulletproof-diet/">Bulletproof Diet Roadmap</a></li>
                        <li id="menu-item-16519" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-16519"><a href="https://blog.bulletproof.com/bulletproof-radio-episodes-directory/">Bulletproof Radio Episode Directory</a></li>
                        <li id="menu-item-16520" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16520"><a href="https://blog.bulletproof.com/30-day-upgrade-resource/">30 Day Upgrade Guide</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="copy">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>Copyright © 2018 Bulletproof 360, Inc. • All Rights Reserved.</p>
                    <a class="r" href="https://www.bulletproof.com/terms-conditions" target="_blank">Terms of Use</a>
                    <a class="r" href="https://www.bulletproof.com/privacy-policy" target="_blank">Privacy</a>
                </div>
            </div>
        </div>
    </div>
</footer>