<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

     <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
      <!-- Ionicons -->
      <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
      <!-- jvectormap -->
      <link rel="stylesheet" href="{{asset('css/jquery-jvectormap.css')}}">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
      <!-- AdminLTE Skins. Choose a skin from the css/skins
           folder instead of downloading all of them to reduce the load. -->
      <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- Styles -->

  
</head>
<body class="hold-transition skin-blue sidebar-mini">

    <div class="wrapper" id="backend">
         @include('layouts.sidebar')
         @include('layouts.header')

       

         <div class="content-wrapper">

            @yield('content')
        </div>
         @include('layouts.footer')
    </div>
    
    <script src="{{ asset('js/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{ asset('js/fastclick.js')}} "></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/adminlte.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{ asset('js/jquery.sparkline.min.js')}} "></script>
<!-- jvectormap  -->
<script src="{{ asset('js/jquery-jvectormap-1.2.2.min.js')}} "></script>
<script src="{{ asset('js/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/jquery.slimscroll.min.js')}} "></script>
<!-- ChartJS -->
<script src="{{ asset('js/Chart.js')}} "></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('js/dashboard2.js')}} "></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('js/demo.js')}}"></script>
</body>
<!-- jQuery 3 -->

</html>
