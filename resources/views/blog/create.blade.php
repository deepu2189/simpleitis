@extends('layouts.backend')

@section('content')
    <!-- Content Wrapper. Contains page content -->

    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create Blog</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="{{ route('blog.store') }}" role="form">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input name="title" type="text" class="form-control" id="title"
                                       placeholder="Enter Title" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Description</label>
                                <textarea required name="description"  class="form-control" id="exampleInputPassword1"
                                ></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Featured image</label>
                                <input type="file" id="exampleInputFile">

                                <p class="help-block">Upload a image for this post</p>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="status" value="1" type="checkbox">Publish
                                </label>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>


            </div>
        </div>
        <!-- /.row -->
    </section>



@endsection
