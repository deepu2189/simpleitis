@extends('layouts.frontend')

@section('content')
    <div id="content-wrap" class="container">
        <div class="row">
            <div id="content">
                <div class="sc">
                    <div class="row spb lat2">
                        <div class="col-md-12">
                            <a class="va" href="https://blog.bulletproof.com/category/article/">View all Articles +</a>
                            <h3>Latest Articles</h3>
                        </div>
                        <div class="flex-pt gridm">
                            @foreach($blogs as $blog)
                            <a class="sub col-md-6" href="#"
                               style="background-image:url(https://44uc8dkwa8q3f5b66w13vilg-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/leftovers_header-734x400.jpg);">
                                <div class="sdw"></div>
                                <div class="ico ib is i-artb ctrm"></div>
                                <div class="con">
                                    <h2 class="w700">{{ $blog->description }}</h2>
                                    <span>Article</span>
                                </div>
                            </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="row spb cur">
                        <div class="col-md-12">
                            <h3>Most Read</h3>
                        </div>
                        <div class="flex-pt gridm">
                            <a class="sub col-md-4"
                               href="https://blog.bulletproof.com/how-to-make-your-coffee-bulletproof-and-your-morning-too/"
                               style="background-image:url(https://44uc8dkwa8q3f5b66w13vilg-wpengine.netdna-ssl.com/wp-content/uploads/2018/07/new-new-bp-header_V1-734x542.jpg);">
                                <div class="sdw"></div>
                                <div class="ico ib is i-artb ctrm"></div>
                                <div class="con">
                                    <h2 class="w700">How to Make Bulletproof Coffee … And Make Your Morning
                                        Bulletproof</h2>
                                    <span>Article</span><span>Featured</span>
                                </div>
                            </a>
                            <a class="sub col-md-4" href="https://blog.bulletproof.com/intermittent-fasting-guide/"
                               style="background-image:url(https://44uc8dkwa8q3f5b66w13vilg-wpengine.netdna-ssl.com/wp-content/uploads/2018/04/The-Incredible-Benefits-Intermittent-Fasting_header.jpg);">
                                <div class="sdw"></div>
                                <div class="ico ib is i-artb ctrm"></div>
                                <div class="con">
                                    <h2 class="w700">The Intermittent Fasting Guide for Beginners</h2>
                                    <span>Article</span><span>Featured</span>
                                </div>
                            </a>
                            <a class="sub col-md-4" href="https://blog.bulletproof.com/survey-win-200-credit/"
                               style="background-image:url(https://44uc8dkwa8q3f5b66w13vilg-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/survey-giftcards-header-734x542.jpg);">
                                <div class="sdw"></div>
                                <div class="ico ib is i-b ctrm"></div>
                                <div class="con">
                                    <h2 class="w700">Enter to Win a $200 Store Credit to Bulletproof.com</h2>
                                    <span></span><span>Featured</span>
                                </div>
                            </a>
                        </div>
                    </div>



                    <div class="row mini">
                        <div class="col-md-4">
                            <h3>More Articles</h3>
                            <div class="shr"></div>
                            <a class="rel wys" href="https://blog.bulletproof.com/dark-roast-coffee-brain-study/">
                                <img src="img/This-Is-Your-Brain-On-Coffee_How-much-coffee-is-too-much-75x75.jpg"
                                     width="68" height="68">
                                <h2>Dark Roast Coffee May Reduce Risk of Alzheimer’s and Parkinson’s, Study Says</h2>
                                <p>Article</p>
                            </a>
                            <a class="rel wys" href="https://blog.bulletproof.com/sugar-cravings-challenge/">
                                <img src="img/Everything-You-Know-About-Cholesterol-Is-Wrong_Avoiding-sugar-and-excess-carbs-75x75.jpg"
                                     width="68" height="68">
                                <h2>The 30-Day No Sugar Challenge: A Step-by-Step Guide To Ending Sugar Cravings</h2>
                                <p>Article</p>
                            </a>
                            <a class="rel wys" href="https://blog.bulletproof.com/gout-diet-natural-remedies/">
                                <img src="img/Why-Gout-is-On-The-Rise-and-How-Your-Diet-Plays-a-Part-in-Symptoms_header-75x75.jpg"
                                     width="68" height="68">
                                <h2>Why Gout Is On The Rise and How Your Diet Plays a Part in Symptoms</h2>
                                <p>Article</p>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <h3>More Podcasts</h3>
                            <div class="shr"></div>
                            <a class="rel wys" href="https://blog.bulletproof.com/bob-hariri-539/">
                                <img src="img/BobHariri-75x75.jpg" width="68" height="68">
                                <h2>100 Years Old is the New 60 – Dr. Bob Hariri #539</h2>
                                <p>Podcast</p>
                            </a>
                            <a class="rel wys" href="https://blog.bulletproof.com/eric-kandel-538/">
                                <img src="img/kandel-75x75.jpg" width="68" height="68">
                                <h2>The Unusual Brain of Dr. Eric Kandel #538</h2>
                                <p>Podcast</p>
                            </a>
                            <a class="rel wys" href="https://blog.bulletproof.com/charles-poliquin-537/">
                                <img src="img/sensei_smile-75x75.jpg" width="68" height="68">
                                <h2>Special Edition: Aerobic Exercise May Be Destroying Your Body – Charles Poliquin
                                    #537</h2>
                                <p>Podcast</p>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <h3>More Recipes</h3>
                            <div class="shr"></div>
                            <a class="rel wys"
                               href="https://blog.bulletproof.com/cranberry-orange-muffins-recipe-keto-2g/">
                                <img src="img/cran-orange-muffins_header-75x75.jpg" width="68" height="68">
                                <h2>Keto Cranberry Orange Muffins</h2>
                                <p>Recipe</p>
                            </a>
                            <a class="rel wys" href="https://blog.bulletproof.com/sweet-potato-gratin-recipe-paleo-2g/">
                                <img src="img/Sweet-Potato-Gratin_header-75x75.jpg" width="68" height="68">
                                <h2>Paleo Sweet Potato Gratin</h2>
                                <p>Recipe</p>
                            </a>
                            <a class="rel wys" href="https://blog.bulletproof.com/air-fryer-recipes-paleo-2g/">
                                <img src="img/21-Air-Fryer-Recipes-for-All-Your-Fried-Food-Cravings_header-75x75.jpg"
                                     width="68" height="68">
                                <h2>21 Air Fryer Recipes for All Your Fried Food Cravings</h2>
                                <p>Recipe</p>
                            </a>
                        </div>
                    </div>
                    <div class="row mdia">
                        <div class="col-md-12">
                            <h3>Media Type</h3>
                        </div>
                        <div class="col-md-12">
                            <a href="https://blog.bulletproof.com/category/article/" class="media-type">
                                <div class="ico imt i-artc ctrm"></div>
                                <span>Articles</span>
                            </a>
                            <a href="https://blog.bulletproof.com/category/podcasts/" class="media-type">
                                <div class="ico imt i-podc ctrm"></div>
                                <span>Podcasts</span>
                            </a>
                            <a href="https://blog.bulletproof.com/category/video/" class="media-type">
                                <div class="ico imt i-vidc ctrm"></div>
                                <span>Videos</span>
                            </a>
                            <a href="https://blog.bulletproof.com/category/infographic/" class="media-type">
                                <div class="ico imt i-infc ctrm"></div>
                                <span>Infographics</span>
                            </a>
                            <a href="https://blog.bulletproof.com/category/recipes/" class="media-type">
                                <div class="ico imt i-recc ctrm"></div>
                                <span>Recipes</span>
                            </a>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="row spb sctag">
                        <div class="col-md-12">
                            <h3>Popular Topics</h3>
                        </div>
                        <div class="tags col-md-12">
                            <a href="https://blog.bulletproof.com/tag/biohacking/">biohacking</a><a
                                    href="https://blog.bulletproof.com/tag/bulletproof-diet/">Bulletproof Diet</a><a
                                    href="https://blog.bulletproof.com/tag/gluten-free/">gluten-free</a><a
                                    href="https://blog.bulletproof.com/tag/sugar-free/">sugar-free</a><a
                                    href="https://blog.bulletproof.com/tag/diet/">Diet</a><a
                                    href="https://blog.bulletproof.com/tag/nutrition/">nutrition</a><a
                                    href="https://blog.bulletproof.com/tag/performance/">Performance</a><a
                                    href="https://blog.bulletproof.com/tag/brain/">brain</a><a
                                    href="https://blog.bulletproof.com/tag/cognitive-enhancement/">cognitive
                                enhancement</a><a href="https://blog.bulletproof.com/tag/vegetarian/">vegetarian</a><a
                                    href="https://blog.bulletproof.com/tag/fat-burning/">fat burning</a><a
                                    href="https://blog.bulletproof.com/tag/healthy-fat/">healthy fat</a><a
                                    href="https://blog.bulletproof.com/tag/stress/">stress</a><a
                                    href="https://blog.bulletproof.com/tag/supplements/">supplements</a><a
                                    href="https://blog.bulletproof.com/tag/fat/">fat</a><a
                                    href="https://blog.bulletproof.com/tag/toxins/">toxins</a><a
                                    href="https://blog.bulletproof.com/tag/sleep/">sleep</a><a
                                    href="https://blog.bulletproof.com/tag/food/">food</a><a
                                    href="https://blog.bulletproof.com/tag/chocolate/">Chocolate</a>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
@endsection
