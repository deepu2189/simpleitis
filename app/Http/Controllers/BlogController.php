<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Mockery\Exception;
use DB;


class BlogController extends Controller
{
    /**
     * Author Deepak Thakur
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * version 1.0
     */
    public function index(Blog $blog){
        $blogs = $blog->all();
        return view('blog.index',compact('blogs'));
    }

    public function create()
    {
        return view('blog.create');
    }

    public function store(Blog $blog)
    {
        try{
            DB::beginTransaction();
            $inputs = request()->all();
            $inputs['user_id'] = userId();
            unset($inputs['_token']);
            if($blog->create($inputs)){
                DB::commit();
                return redirect(route('blog.index'));
            }
        }
        catch (Exception $e){
            DB::rollback();
        }
    }


}
